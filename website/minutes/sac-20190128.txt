Minutes of the Solidary Administrative Council meeting of 2019-01-28


Present: Christopher Baines, Ludovic Courtès, Andreas Enge,
Manolis Ragkousis, Ricardo Wurmus


1) Supporting the Guix Days 2019 in Brussels

The following is decided unanimously:
- Guix Europe contributes 600€ towards the organisation of the Guix Days
  on January 31st and February 1st 2019 in Brussels, of which 400€ correspond
  to donations received for this purpose by GeneNetwork and Joy of Source,
  and 200€ come out of the Guix Europe funds.
- Guix Europe advances the remaining costs of the Guix Days organisation,
  as agreed by the Guix spending committee.
- These remaining costs are to be reimbursed by the Guix funds held at
  the FSF, with potential banking fees (which last time were 15€) being
  borne by Guix Europe.

This arrangement is also approved unanimously by the Guix spending committee.


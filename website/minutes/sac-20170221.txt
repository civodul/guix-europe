Minutes of the Solidary Administrative Council meeting of 2017-02-21


Present: Andreas Enge, Mathieu Lirzin, Cyril Roelandt;
by electronic vote: Ludovic Courtès


1) Membership to Aquilenet

Aquilenet suggests that Guix Europe become a member of the Aquilenet
association, for a yearly fee of 20€, which would simplify billing the
membership fees. This is agreed upon unanimously.


2) Domain guixsd.org

The domain is currently registered by Andreas at OVH, and has been renewed
on January 29 for a fee of 15€59. It is decided unanimously that Andreas is
reimbursed of the amount, and that he should try to modify the whois entry
so that it points to Guix Europe.


3) Membership request

The membership request by Theodoras Foradis is accepted unanimously.


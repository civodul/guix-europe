Minutes of the General Assembly of 2022-07-17

Present during the online meeting on BigBlueButton:
Andreas Enge, Manolis Ragkousis, Efraim Flashner, Jonathan Brielmaier, Julien Lepiller, Ludovic Courtès

By email:
Ricardo Wurmus, Simon Tournier


The assembly starts as an Extraordinary General Assembly.

1. Changes in the statutes

The Extraordinary General Assembly decides unianimously to apply the
typographical changes suggested by Simon.


The assembly continues as an Ordinary General Assembly.

2. Website on foundation.guix.info

Creation of the website should be automated, either by using a Guix service
or the Gitlab CI. Then it should be moved to bayfront. SAC members should
be added. Guix Days wikis should also be copied. It would be nice to
integrate the ledger balance into the website as well.

Artwork/pictures to replace the fish: Look in the artwork-guix repository,
in particular the release announcement pictures "Flight of the Guix",
"Guix Pilot".
Ask Luis Felipe whether they can create a logo or even a mascot.

We should send an announcement to the guix-devel mailing list once the
migration is finished.


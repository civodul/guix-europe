Minutes of the Solidary Administrative Council meeting of 2023-09-11


Present: 

Adriel Dumas--Jondeau, Andreas Enge, Christopher Baines, David Wilson
Efraim Flashner, Jonathan Brielmaier, Julien Lepiller, Manolis Ragkousis
Oliver Propst, Pjotr Prins, Ricardo Wurmus, Simon Tournier, Tangy Le Carrour
Timo Wilken


1) Membership request

The membership request by Fabio Natali is accepted unanimously.


Compte-rendu de l’assemblée générale extraordinaire du 26 mai 2023

Présences :
	* Andreas Enge
	* Christopher Baines (représenté par Andreas Enge)
	* Simon Tournier
	* Oliver Probst
	* Adriel Dumas--Jondeau
	* Tanguy Le Carrour (représenté par Adriel Dumas--Jondeau)
	* Timo Wilken
	* Bertrand Mathelier
	* Pjotre Prins
	* Julien Lepiller
	* Josselin Poiret
	* Pukkamustard (représenté par Indieterminacy)
	* Indieterminacy
	* Jlicht


Personnes présentes ou représentées : 14 sur 27 membres.  Le quorum (18 personnes) n’est pas atteint ; l’Assemblée continue comme Assemblée Générale ordinaire.

1. Rapport d’activité du président

  Simon présente le rapport suivant des activités de Guix Europe
  depuis le précédent.  D’ailleurs, notons que c’est son premier
  rapport en tant que président.

  * Guix a fêté ses dix ans en 2022. https://10years.guix.gnu.org

    . Guix Europe s’est impliqué dans l’organisation de l’évènement du
    16 au 18 septembre 2022 à Paris (France). Tout d’abord, Guix
    Europe a apporté son soutien financier à l’évènement. De plus
    l’association a facilité le rapport entre mécènes et paiement des
    factures ; le don tardif d’un mécène a été reçu ce mois de Mai
    (être facilitateur financier n’est pas de tout repos!) Finalement,
    plusieurs membres ont contribué du temps à la logistique.

    . Guix Europe a été évoqué lors de deux conférences :

      + Dix années d’échecs (Ten Years of failure)
        https://10years.guix.gnu.org/video/ten-years-of-failures/
      + Guix Europe, comment soutenir le projet Guix (Guix Europe, how to support the Guix project)
        https://10years.guix.gnu.org/video/guix-europe-how-to-support-the-guix-project/

    . De nouveaux membres ont adhéré suite à l’évènement.

    . Un des membres de l’association a compilé 10 années d’histoires Guix.
    https://guix.gnu.org/en/blog/2022/10-years-of-stories-behind-guix/

  * Le site internet est en fonctionnement et accessible depuis
    http://foundation.guix.info

    . Des illustrations et logos ont été discutés par le Collège
    d’Administration Solidaire le 25 octobre 2022 à 12:44. Luis Felipe
    en propose gratieusement 4 variantes. Le résultat du vote du
    changement de nom est attendu pour la suite.

    . Il a été évoqué lors de l’Assemblée Générale du 17 juillet 2022
    la question de la construction et de la maintenance du site
    internet. Aucune action n’a été prise à ce propos, et deux actions
    sont en cours :

      + Déplacer le site de la machine personnelle d’Andreas
      + Corriger la configuration pour les certificats

  * Participations à des évènements

    . Nous n’avons pas été présents au Capitole du Libre à Toulouse
    (France). Certains membres ont souhaité demander et tenir un stand
    sur Guix. Nous avons fait un appel à participation de dernière
    minute auquel suffisamment de membres ont répondu, mais nous
    n’étions malheureusement pas synchronisés et avons donc raté la
    date limite. D’ailleurs, des membres de Guix Europe étaient
    présents.

    Nous devons participer à plus d’évènements.

    . La demande de stand au FOSDEM (Bruxelles, Belgique) a été
    rejetée à nouveau.

  * Autour du FOSDEM (Bruxelles, Belgique)

    . Guix Days avant le FOSDEM
    https://libreplanet.org/wiki/Group:Guix/FOSDEM2023

    L’organisation de cet évènement est désormais régulière.  Les deux
    jours précédants le FOSDEM, la communauté Guix se rencontre pour
    discuter et bidouiller.  Guix Europe aide à l’organisation :
    soutien financier, facilitation financière et logistique.
    L’évènement a eu lieu à l’ICAB, comme les années précédentes.

    Plus de 37 personnes présentes.

    . Des membres de Guix Europe ont aidé à l’organisation d’une salle
    thématique (devroom) pour une informatique déclarative et minimaliste
    (Declarative and Minimalist Computing devroom). L’évènement fut un franc
    succès.

    https://fosdem.org/2023/schedule/track/declarative_and_minimalistic_computing/

  * Communauté

    . Aucun évènement en ligne n’a été organisé cette année.

    . Un petit (3 personnes motivées!) Meetup Guix a démarré à
    Paris. D’autres initiatives locales vont (ou ont) peut-être
    démarrées également ?

    . « Where Is Everyone » est une autre communauté autour de
    Guix. Plusieurs membres de Guix Europe y participent. Un
    rapprochement est il envisageable ?

Vote du rapport : 14 oui, 0 non, 0 blanc

2. Rapport financier du trésorier

  Voici le bilan financier de toutes nos opérations depuis notre formation en 2016.

              €4625.01  Ressources:Banque
             €32482.58  Dépenses
                €19.00    Frais bancaires
              €6350.51    Conférences
               €190.88    Domaines
              €7643.70    Guix Day
              €6239.23    Hébergement
                €44.00    Légal
               €140.00    Adhésion
                €60.00    Matériel promotionnel
             €11795.26    Achats
            €-37107.59  Revenus
             €-6105.16    Conférences
             €-9240.00    Dons
             €-5281.00    Guix Day
             €-5714.23    Hébergement
               €-28.39    Intérêts
             €-1270.00    Adhésion
               €-80.00      2016
              €-110.00      2017
              €-120.00      2018
              €-170.00      2019
              €-170.00      2020
              €-160.00      2021
              €-200.00      2022
              €-260.00      2023
             €-9468.81    Achats
  --------------------
                     0

(Nous utilisons un système de comptabilité à double-entrée et en résumé, les
opérations se compensent toujours.)

Avec l’adhésion à 10 € par an, nous observons une augmentation de 8
membres en 2016 à 26 en 2023 avec un plateau pendant la période
« Covid-19 ». Quelques nouveaux membres adhèrent généralement lors des
évènements en personne. La situation est néanmoins dynamique, certains
membres quittent la communauté après quelques années, et en 2022 le
Collège d’Administration Solidaire a révoqué l’adhésion de trois
personnes n’ayant pas payé leurs adhésions deux années consécutives.

La première ligne indique que nous possédons 4625.01 € en banque. En
2019, nous avons ouvert un compte épargne (Livret A) au taux d’intérêt
en vigueur en France (3 % depuis février). Je conserve généralement
les liquidités dans ce compte d’épargnes, sauf quelques centaines
d’euros qui restent sur le compte courant pour un débit mensuel de
75 € à Aquilenet pour l’hébergement de la machine/serveur bayfront.

Voici le bilan financier depuis notre précédente Assemblée Générale
ordinaire le 06 novembre 2021. Nous n’avions pas évoqué la trésorerie
lors de deux précédentes Assemblées Générales Extraordinaires en 2022.

             €2343.36  Ressources:Banque
             €18076.50  Dépenses
                 €4.00    Frais bancaires
              €6350.51    Conférences
              €2656.00    Guix Day
              €1350.00    Hébergement
                €40.00    Adhésion
              €7675.99    Achats
            €-20419.86  Revenus
             €-6105.16    Conférences
             €-3550.00    Dons
             €-1033.00    Guix Day
             €-1500.00    Hébergement
               €-25.71    Intérêts
              €-530.00    Adhésion
               €-30.00      2020
               €-50.00      2021
              €-190.00      2022
              €-260.00      2023
             €-7675.99    Achats
  --------------------
                     0

Notons que toutes les dépenses ont un signe positif, et que tous les
revenus un signe négatif.

La plupart de nos activités sont financièrement neutres car remboursées par la
« Free Software Foundation ». Par exemple l’achat de machines pour la ferme de
compilation, à hauteur de 7675.99 €, a été remboursée par la FSF :

  3 Machines Honeycomb et leurs SSDs : 3766.22 €
  6 SSDs pour la ferme de Berlin :     3909.77 €

2 machines Honeycomb supplémentaires ont été directement payées par la
FSF sans apparaître dans nos soldes.

Ceci est également vrai pour les coûts d’hébergement de bayfront :
nous sommes généralement remboursés par la FSF une fois l’an ou bien
au même moment qu’un autre remboursement matériel pour minimiser les
frais bancaires. Nous avons donc payé 1350 € et reçu 1500 € depuis
septembre 2022.

Les frais d’adhésion sortants de 40 € correspondent aux deux années
d’adhésion à Aquilenet, prérequis pour l’hébergement de bayfront.

L’évènement majeur, en septembre 2022 a été l’anniversaire des 10 ans
de Guix. Les dépenses correspondent principalement aux frais de
restauration. Nous avons également acheté, pour 170.40 €, 2 kakemonos
pour la publicité de l’évènement, et qui serviront à de futures
occasions.

En 2023 nous avons organisé les Guix Days en personne, à nouveau à Bruxelles,
les premiers après l’interruption dûe à la Covid-19. Les dépenses de 2656.00 €
sont la location de la salle et les frais de restauration à l’ICAB. Le revenu
de 1033.00 € correspond à la collecte effectuée au repas. Nous demandons
habituellement à la FSF de rembourser la différence, mais cela n’a pas encore
été fait : le coût restant de 1623 € est pour l’instant à notre charge.

Des dons ont été reçu par des institutions académiques intéressées par
Guix : 1000 € ont été versés par l’université de Grenoble et de
Montpellier respectivement, et 1500 € par l’INRIA. Dans l’ensemble nos
activités sont bénéficiaires d’un gain net de 2323.36 €.

Vote : 14 oui, 0 non, 0 blanc

Jlicht quitte l’assemblée.

3. Élection du président et du trésorier

Candidats :
	* Président : Simon Tournier
	* Trésorier : Julien Lepiller
	* Vice-président : Oliver Probst
	* Vice-trésorier : Andreas Enge

Vote : 13 oui, 0 non, 0 blanc

4. Élection du Collège d’Administration Solidaire

	* Adriel Dumas--Jondeau
	* Andreas Enge
	* Christopher Baines
	* Efraim Flashner
	* Jonathan Brielmaier
	* Julien Lepiller
	* Manolis Ragkousis
	* Oliver Propst
	* Pjotr Prins
	* Ricardo Wurmus
	* Simon Tournier
	* Tanguy Lecarrour
	* Timo Wilken

Vote : 13 oui, 0 non, 0 blanc

5. Changement de nom : Guix Europe

Le ressenti général est en faveur du changement pour « Guix
Foundation », notamment afin d’éviter toute connotation
géographique. Nous ne sommes cependant pas une fondation selon les
lois françaises.

Différentes propositions :

	* Guix 4 All
	* Guix assoc
	* Guix Cabale
	* Guix Europe
	* Guix Foundation
	* Guix Hell
	* Guix Institute
	* Guix Organization
	* Guix PAC (comme la Pompe À Chaleur)
	* Guix Sabbath
	* Guix Society
	* Guix Undefined
	* Guix Undelimited
	* Guix Union
	* Guix United
	* Pssst! Guix
	* The Guix Consortium
	* Xiug

6. Divers
   
  La question de rencontres régulières a été abordée. Nous pourrions
  les prévoir par le Collège d’Administration Solidaire. Les
  objectifs pourraient être la planification des évènements auxquels
  participer, la maintenance du site internet, etc. Des rencontres en
  Juin, en Septembre puis tous les deux mois ont été évoquées. Un
  sujet doit être prévu et l’évènement peut être ouvert au public. De
  plus, ces rencontres peuvent être en personne, comme lors des Guix
  Days.

  Une discussion s’ensuit sur les rencontres en personne comme les
  Guix Days, comme avec l’évènement des 10 ans. Indieterminacy, Timo
  et Adriel sont interessés pour aider à en organiser une, par exemple
  à Londres. Elle pourrait être associée à une rencontre du Fédiverse,
  ou bien avec un autre évènement. Nous pouvons également imaginer un
  « Guix Camp ». Cela serait possible, non sans effort, en Novembre
  2023 si un petit groupe de personne souhaite l’organiser d’ici la
  prochaine assemblée en juin.

Une Assemblée Générale Extraordinaire se tiendra, comme prévu, le 10
Juin à 19 h 00 pour voter la proposition de changement de nom.

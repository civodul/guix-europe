Minutes of Extraordinary General Assembly of 10 June 2023.

This assembly is subsequent of the Ordinary General Assembly of 2023-05-26.
The assembly was united but the quorum had not been reached therefore the
assembly is united again fifteen days later; applying Article 12 of the
statutes.

Present: Tanguy Le Carrour, Andreas Enge, David Wilson, Jelle Licht, Timo
Wilken, Adriel Dumas--Jondeau, Bertrand Mathelier, Simon Tournier, Josselin
Poiret, Christopher Baines, Efraim Flashner (from point 2 on)

1. Name change

Simon points out that there is a number of French "associations loi 1901"
which have "Foundation" in their name, so this is a possibility. It is decided
unanimously to change the name of the "Guix Europe" association to "Guix
Foundation".

In case this name gets rejected by the French administration, it is decided
unanimously to choose "Guix Together" as the alternative name.

Should we change the mailing lists? There is consensus to change once the name
change is official.

Efraim arrives.

2. New members of the SAC

Bertrand, David and Jelle are candidates; the assembly votes unanimously to
add them.

David and Christopher leave.

3. Miscellanea

 - Organizing Guix Days in autumn.  People discussed with fediverse folk.  One
   question is about the money (sponsor?) for organization such event.  One
   question is about the location, especially inside or not the Schegen area;
   there is no consensus about it.  There is a suggestion about an online
   event.  The first question is about the volunteers for organizing.  We
   could have an event more casual than the 10 Years Event in Paris; just an
   event to meet altogether. It could be in conjunction with a potential
   scientific event in Montpellier in November. Timo still volunteers to take
   part in the organisation, but needs help.

 - Organizing Meet's up. There will be one in September. It could be about
   hacking on the website.

 - The website needs love (CSS, etc.).  For example, reporting the accounting
   balance by the static generation.

Minutes of the ordinary General Assembly of 2021-11-06

Present during the online meeting on BigBlueButton:
Andreas Enge, Efraim Flashner, Julien Lepiller, Manolis Ragkousis,
Oliver Propst, Pjotr Prins, Simon Tournier;
7 out of 19 members, the quorum of 2/3 for the Extraordinary Assembly
is not reached


1) Activity report by the Presidency

   Manolis presents the following activites of Guix Europe since the
   last General Assembly in June 2020.

   * Mini online Guix Days at the end of 2020
   * Online FOSDEM dev room; speakers who would not normally attend,
     about 60 attendees
   * Online version of Guix Days right after FOSDEM, about 70 attendees


2) Financial report by the Treasury

   Andreas comments the numbers available in the public ledger at
      https://git.savannah.gnu.org/cgit/guix/maintenance.git/tree/guix-europe/accounting/accounting.ledger
   Most of the money available for Guix activities is held at the FSF;
   the only financial transactions by Guix Europe since the last General
   Assembly were advancing money for hosting, which is reimbursed about
   once or twice a year by the FSF; receiving membership fees; paying our
   membership at Aquilenet; and paying for five years of registering the
   domain guix.info.
   The current balance of our accounts is close to 3000€.


3) Election of the new Presidency and Treasury

   Manolis wishes to step down after almost four years of Presidency; we
   express our gratitude for all his work to promote Guix and Guix Europe,
   and will take him by his word to co-organise future events.
   The following persons stand for election:
   - Presidency: Simon Tournier
   - Co-presidency: Oliver Propst
   - Treasury: Andreas Enge
   - Co-treasury: Julien Lepiller
   All four are elected unanimously.


4) Election of the Solidary Administrative Council (SAC)
   
   As members of the Solidary Administrative Council, the General Assembly
   unanimously elects the four persons already mentioned at 3) as well as
   the other three members present, Efraim Flashner, Manolis Ragkousis and
   Pjotr Prins.

   More members can be part of the SAC; we decide to sollicit applications
   to be voted upon at our next Extraordinary General Assembly.


5) Travel grants

   We could propose travel grants once physical meetings start again. A
   limited amount of money could come from Guix Europe itself, and more
   money could come from the funds held at the FSF assuming that the
   Guix spending committee agrees.

6) Fundraising

   Simon suggests to look into making Guix Europe a tax deductible entity
   in France ("association d'intérêt général"), which could help attract
   donations. Depending on local legislation, tax deductions may also be
   possible for donations made from other European countries, and in
   particular also by businesses.


7) Possiblity of a future digital or physical Guix meetup

   * FOSDEM 2022 will be online on February 5 and 6, 2022. Julien, Manolis
     and Pjotr volunteer to organise the Guix devroom; the application
     deadline is November 15.
   * Guix Days after FOSDEM. We could organise a physical meeting
     potentially in spring, hoping that the Covid situation will be
     mastered by then.

8) Future activities

   * We need a web page, for instance on association.guix.info. Oliver and
     Simon volunteer to work on this.
   * We could have a logo, but need to ask for assistance.
   * Oliver suggests to write a post to promote Guix Europe membership.
   * Oliver suggests to write a blog post for marketing Guix outside its
     current core community.
   * Success stories on deployments could fit blog posts as well, and these
     are hardly covered by the normal Guix blog.
   * There is need for hosting and administrating hardware, members can
     volunteer for this.
   * We discuss ideas about changing our name, to make it less centred on
     Europe, while the association will continue to be registered in France.
     Suggestions are: Guix Together (that is already the name of a
     gathering in Miami, we should ask their permission; Simon volunteers
     to drop them an email). But keeping Guix Europe also has its fans.
     We should start a discussion on the mailing list.
   * We should check whether we can open a bank account on Wise to accept
     membership fees in other currencies than euros.


Minutes of the Solidary Administrative Council meeting of 2023-02-18


Present: Andreas Enge, Julien Lepiller, Christopher Baines,
Efraim Flashner, Jonathan Brielmaier, Oliver Propst, Simon Tournier,
Ricardo Wurmus


1) Membership requests

The membership requests by Samuel Fadel, Timo Wilken, Jgart, Pukkamustard,
Josselin Poiret, Bertrand Mathelier and Gábor Boskovits are accepted
unanimously.


Present: Andreas Enge, Pjotr Prins, Ricardo Wurmus, Simon Tournier,
Efraim Flashner, Julien Lepiller


2) Membership removal

It is decided unanimously to revoke the membership of a member who has not
paid the membership dues of 2022 and whose email address bounces.


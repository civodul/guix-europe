Minutes of the extraordinary General Assembly of 2023-05-26

Present:
	* Andreas Enge
	* Christopher Baines (represented by Andreas Enge)
	* Simon Tournier
	* Oliver Probst
	* Adriel Dumas--Jondeau
	* Tanguy Le Carrour (represented by Adriel Dumas--Jondeau)
	* Timo Wilken
	* Bertrand Mathelier
	* Pjotre Prins
	* Julien Lepiller
	* Josselin Poiret
	* Pukkamustard (represented by Indieterminacy)
	* Indieterminacy
	* Jlicht

Present or represented: 14 out of 27, the quorum of 18 is not reached.  We
continue as an ordinary General Assembly.


1. Activity report by the Presidency

  Simon presents the following activities of Guix Europe since the last
  report.  Aside, let note that it is his first report as Presidency.

  * 2022 was the Ten Years of Guix. https://10years.guix.gnu.org

    . Guix Europe had been involved in the organization of the event on 16–18
    September 2022 in Paris, France.  Firstly, Guix Europe provided a
    financial support to the event.  Moreover, the association were the
    financial facilitator between the sponsors and the payment of the bills;
    late donation from a sponsor had been done this month of May -- being the
    facilitator is not a smooth business.  Last, several members helped in the
    logistic of the event.

    . Guix Europe had been presented in two talks:

      + Ten Years of failure
        https://10years.guix.gnu.org/video/ten-years-of-failures/
      + Guix Europe, how to support the Guix project
        https://10years.guix.gnu.org/video/guix-europe-how-to-support-the-guix-project/

    . The outcome of this event had been new members.

    . A member of Guix Europe collected 10 Years of stories behind Guix.
    https://guix.gnu.org/en/blog/2022/10-years-of-stories-behind-guix/

  * The website is now running and served on http://foundation.guix.info

    . Some artwork and logos are been discussed by Guix Europe SAC on 25
    October 2022, 12:44.  Luis Felipe is generously proposing 4 variants.  The
    situation is waiting the vote for the rename.

    . The previous General Assembly of 2022-07-17 discussed technical details
    for building and maintaining the website.  Nothing had been done on this
    front.  Two actions are pending:

      + Move ton elsewhere instead of personal Andreas's machine
      + Fix the configuration for the certificates

  * Participate in events

    . We missed the opportunity in Capitole du Libre (Toulouse, France).
    Members volunteered for submitting a request and being at some stand about
    Guix.  We did a last minute call and we were enough members but poorly
    synchronized, thus we missed the deadline.  By the way, members of Guix
    Europe were there.

    We need to participate in more events.

    . The request for a stand (booth) in FOSDEM (Brussels) had been rejected,
    again.

  * Around FOSDEM (Brussels, Belgium)

    . Guix Days before FOSDEM
    https://libreplanet.org/wiki/Group:Guix/FOSDEM2023

    The organization of this event is now regular.  The two days before
    FOSDEM, the Guix community is meeting for discussing and hacking.  Guix
    Europe helps in organizing: financial support, financial facilitator,
    logistic.  The event took place in ICAB as the previous editions.

    Participants 37+.

    . Members of Guix Europe helped in organizing the Declarative and
    Minimalist Computing devroom.  This devroom had been a great success.

    https://fosdem.org/2023/schedule/track/declarative_and_minimalistic_computing/

  * Community

    . No online event had been organized this year.

    . A small (3 motivated people) Guix Meetup has started located in Paris.
    Maybe, other local initiative also started?

    . "Where Is Everyone" is another community focused on Guix.  Some members
    of Guix Europe participated in.  Maybe, we could come closer together.

Vote: 14 yes, 0 no, 0 blank


2. Financial report by the Treasury

  The following summarises all our operations since our foundation in 2016.

              €4625.01  Assets:Bank
             €32482.58  Expenses
                €19.00    Banking
              €6350.51    Conferences
               €190.88    Domains
              €7643.70    Guix Day
              €6239.23    Hosting
                €44.00    Legal
               €140.00    Membership
                €60.00    Promotional material
             €11795.26    Purchases
            €-37107.59  Income
             €-6105.16    Conferences
             €-9240.00    Donations
             €-5281.00    Guix Day
             €-5714.23    Hosting
               €-28.39    Interests
             €-1270.00    Membership
               €-80.00      2016
              €-110.00      2017
              €-120.00      2018
              €-170.00      2019
              €-170.00      2020
              €-160.00      2021
              €-200.00      2022
              €-260.00      2023
             €-9468.81    Purchases
  --------------------
                     0

  With 10€ of membership fees per year, you see that we have gone from 8
  members in 2016 to 26 members in 2023, with a plateau during the COVID19
  time. Usually every in-person event brings a few new members. Nevertheless
  the situation is dynamic, some members leave the Guix community after a few
  years, and in 2022 the Solidary Administrative Council revoked the
  membership of two persons who had not paid their membership fees for three
  years, and in 2023 it revoked the membership of three persons who had not
  paid their membership fees for two years after contacting them.

  The first line shows that we currently have €4625.01 in the bank.  In 2019
  we have opened a savings account ("Livret A"), with an interest rate fixed
  by the French state, of 3% now since February. I usually keep most of the
  money in the savings account, with a few hundred euros in the checking
  account to provide for the monthly debit of €75 by Aquilenet for hosting
  bayfront.

  The following summarises the financial operations since our last ordinary
  General Assembly on 2021-11-06; we had two more Extraordinary ones in 2022,
  but did not discuss financial matters then.

              €2343.36  Assets:Bank
             €18076.50  Expenses
                 €4.00    Banking
              €6350.51    Conferences
              €2656.00    Guix Day
              €1350.00    Hosting
                €40.00    Membership
              €7675.99    Purchases
            €-20419.86  Income
             €-6105.16    Conferences
             €-3550.00    Donations
             €-1033.00    Guix Day
             €-1500.00    Hosting
               €-25.71    Interests
              €-530.00    Membership
               €-30.00      2020
               €-50.00      2021
              €-190.00      2022
              €-260.00      2023
             €-7675.99    Purchases
  --------------------
                     0
  Note that all expenses have a positive sign, all spendings a negative sign.

  Most of our activities were financially neutral, since they were reimbursed
  by the Free Software Foundation.  For instance, purchases of machines for
  the build farms for €7675.99 were eventually reimbursed by the FSF:

  - 3 Honeycomb machines and their SSDs: €3766.22
  - 6 SSDs for the berlin build farm:    €3909.77

  2 more Honeycomb machines were directly paid by the FSF without appearing in
  our balances.

  The same holds essentially for the hosting costs of bayfront; we usually ask
  the FSF to reimburse them once a year, or at the same time as another
  reimbursement for hardware to save on banking fees. So during the period, we
  paid €1350 and received €1500, up to and including September 2022.

  The outgoing membership fees of €40 correspond to two years of membership at
  Aquilenet, a prerequisite for them hosting bayfront.

  The 10 Years of Guix celebrations were a major conference in September 2022.
  Expenses correspond mainly to catering. We also spent €170.40 on two
  kakemonoes advertising the event, and which will be available for future
  activities.

  In 2023, we have organised the first in-person Guix Days in Brussels again
  after the covid interruption. The expenses of €2656.00 are the rent of the
  venue and catering at ICAB, while the income of €1033.00 is the money
  collected at lunch. We usually ask the FSF to reimburse the difference,
  which still needs to be done: so far the cost of €1623 is borne by us.

  Donations have been received by academic institutions interested in Guix:
  €1000 each by the Universities of Grenoble and Montpellier, and €1500 by
  INRIA. So altogether our activities have resulted in a net gain of €2323.36.

Vote: 14 yes, 0 no, 0 blank

Jlicht leaves.


3. Election of the new Presidency and Treasury

Candidates:
Presidency: Simon Tournier
Treasury: Julien Lepiller
Vice-presidency: Oliver Probst
Vice-treasury: Andreas Enge

Vote: 13 yes, 0 no, 0 blank


4. Election of the Solidary Administrative Council (SAC)

	* Adriel Dumas--Jondeau
	* Andreas Enge
	* Christopher Baines
	* Efraim Flashner
	* Jonathan Brielmaier
	* Julien Lepiller
	* Manolis Ragkousis
	* Oliver Propst
	* Pjotr Prins
	* Ricardo Wurmus
	* Simon Tournier
	* Tanguy Lecarrour
	* Timo Wilken

Vote: 13 yes, 0 no, 0 blank


5. Name change: Guix Europe

  The general feeling is in favour of switching to Guix Foundation, to drop
  the geographic association. But we are not a "fondation" according to French
  law.

  Different proposals:

	* Guix 4 All
	* Guix assoc
	* Guix Cabale
	* Guix Europe
	* Guix Foundation
	* Guix Hell
	* Guix Institute
	* Guix Organization
	* Guix PAC (this means "heat pump" in French - "pompe à chaleur")
	* Guix Sabbath
	* Guix Society
	* Guix Undefined
	* Guix Undelimited
	* Guix Union
	* Guix United
	* Pssst! Guix
	* The Guix Consortium
	* Xiug

6. Miscellanea

  There is a question whether we have regular meetings; we could have them for
  the SAC. They could be used for things like planning for events to attend,
  online events or working on the website. Meetings in June, September, and
  then every two months are decided. They should have a topic and could be
  open to the public. Additionally we could have in-person meetings at events
  such as the Guix Days.

  A discussion ensues about having an in-person event between two Guix Days,
  like the 10 Years event. Indieterminacy, Timo and Adrien are interesting in
  helping to organise it, for instance in London. It could be in conjunction
  with a fediverse meeting, or with another event. It could also be a "Guix
  camp". November 2023 would be feasible with some effort, if a small group of
  people willing to take the organisation in their hands steps up until the
  next assembly in June.


An extraordinary General Assembly will take place, as announced, on June 10 at
19h to vote on the proposed name change.

title: Assets
date: 2023-11-23 0:00
---
## Finances

Our [financial
status](https://framagit.org/guix-europe/guix-europe/-/blob/main/accounting/accounting.ledger)
is public and permanently updated in a
[ledger](https://www.ledger-cli.org/) file.


## Advertising material

- 2 kakemonoes, one at Andreas's, one with Tanguy at
  [Easter-eggs](https://www.easter-eggs.com/)'s offices.
- `guix.info` domain

## Machines

Guix Europe owns some hardware, usually hosted by members, to take part in
operating the
[bordeaux](https://bordeaux.guix.gnu.org/activity)
and the
[berlin](https://ci.guix.gnu.org/workers)
[build farms](https://qa.guix.gnu.org/branch/master).


### Bayfront

Among other uses, head of the bordeaux build farm, hosted at
[Aquilenet](https://www.aquilenet.fr/).


### Overdrive

6
[Softiron Overdrive
1000](https://github.com/SoftIron/overdrive/tree/master/1000)
- [dover](https://ci.guix.gnu.org/machine/dover)
  hosted by Chris, berlin build farm
- [overdrive1](https://ci.guix.gnu.org/machine/overdrive1)
  hosted by Ludo, berlin build farm
- [lieserl](https://ci.guix.gnu.org/machine/lieserl)
  hosted by Julien, berlin build farm
- monokuma hosted by Chris, bordeaux build farm
- sergei hosted by Tobias, offline
- dmitri hosted by Tobias, offline


### Honeycomb

5
[Honeycomb
LX2](https://www.solid-run.com/arm-servers-networking-platforms/honeycomb-servers-workstation/)
boards
- [pankow](https://ci.guix.gnu.org/machine/pankow),
  hosted at MDC, berlin build farm
- [grunewald](https://ci.guix.gnu.org/machine/grunewald),
  hosted at MDC, berlin build farm
- [kreuzberg](https://ci.guix.gnu.org/machine/kreuzberg),
  hosted at MDC, berlin build farm
- hamal,
  hosted by Chris, bordeaux build farm
- hatysa,
  hosted by Chris, bordeaux build farm


### Novena

1 [Novena](https://www.kosagi.com/w/index.php?title=Novena_Main_Page)
board
- redhill and an external hard drive, hosted by Andreas, offline


title: GNU Guix/Guile Day FOSDEM 2018
date: 2022-11-13 0:00
---
This page was for tracking anything related with the 2018 GNU Guix/Guile
conference and hackathon on Friday Feb 2nd - the day before
[FOSDEM 2018](https://fosdem.org/2018/).
This event was organised by the European Guix Foundation as a
[FOSDEM 2018 fringe event](https://fosdem.org/2018/fringe/).

## Why?

GNU Guix is growing rapidly and has gone from a software packaging
system to a full tool stack aimed at reproducible software deployment
and development. GNU Guix is a toolkit that allows developers to
integrate reproducible software deployment into their applications---as
opposed to leaving it up to the user. GNU Guix is based on the GNU Guile
programming language which makes it a very versatile and hackable (in
the good sense) environment.

## When?

Fri 2 February 2018, from 9:00 hrs - 18:00 hrs.

## Where?

[ICAB](http://icab.be/) - The Institute of Cultural Affairs in Belgium\
Rue Amedee Lynen 8\
1210 Brussels, Belgium

## Programme

The morning is for short talks (20 min. max). If you want to give a talk
contact Manolis, Pjotr or Ludo (see below).

Current list of talks:

- GNU Guix state of affairs - by Ludovic Courtès
- GNU Guix in an HPC environment by Ricardo Wurmus
- The future of Nix by Eelco Dolstra
- A fully compile-from-source bootstrap of software distributions
- Using GNU Guix for software development and distributing software by
  Pjotr Prins
- The GNU Workflow language (GWL) by Roel Janssen
- Panel discussion with above speakers on GNU
  Guix progress and 2018 priorities

Next:

- Talk, discuss, hack and more talk, discuss, hack

Last minute discussions, live hacking and lightning talks/presentations
(10 min. max) are welcomed too!

## Code of conduct

Attendees implicitely abode by the [code of conduct as
stated by FOSDEM](https://fosdem.org/2018/practical/conduct/).

## Attendees

1.  Pjotr Prins
2.  Manolis Ragkousis
3.  Alex Sassmannshausen
4.  Gábor Boskovits
5.  Christopher Baines
6.  Efraim Flashner
7.  Julien Lepiller
8.  Thomas Danckaert
9.  Ricardo Wurmus
10. Roel Janssen
11. Ludovic Courtès
12. Mark Meyer
13. Walid Shaari
14. Amirouche Boubekki aka. amz3
15. Tobias Geerinckx-Rice
16. Chris Marusich
17. Devan (dvn)
18. Eelco Dolstra
19. Jelle Licht (after 13.00)
20. Michiel Hegemans (after 13.00)
21. Mehmet Erdem
22. Carlo Zancanaro
23. Danny Milosavljevic (after 13.00)
24. Andreas Enge
25. Björn Höfling
26. Alex ter Weele
27. Nigel Tummers
28. Jérémy Korwin
29. Leo Famulari
30. Tobias Platen
31. Profpatsch
32. Claes Wallin

## Costs

Attendance is free. We asked a contribution for consumptions.

## Sponsors

- Guix Europe
- [GeneNetwork](http://gn2.genenetwork.org/)


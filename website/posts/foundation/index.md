title: Guix Foundation
date: 2023-09-17 0:00
---
Guix Foundation is a non-profit association according to French law
(« loi 1901 »),
[registered in 2016](/statutes/JOAFE_PDF_Unitaire_20160008_00760.pdf)
and with the SIRET number 917 723 066 00010.
Its aim is to provide a legal entity, independent of
any individuals working on the
[GNU Guix](https://guix.gnu.org/)
project, that can receive donations
(especially in Euro, complementing the
[FSF fundraising campaigns](https://my.fsf.org/civicrm/contribute/transact?reset=1&id=50)),
host activities and support the project in any conceivable way.

We own and host part of the infrastructure for the Guix build
farm and help fund and organise
[events](/events/index.html) around Guix.

The current board is composed of
[Simon Tournier](https://simon.tournier.info) as Presidency and
[Julien Lepiller](mailto:xxx) as Treasury.
While well-identified public representatives of the association are
required by French law, our aim is to have a collegiate and
collective governance. In fact, all decisions are taken by the
Solidary Administrative Council, to which all members of the
association wishing to take responsability for its functioning
can be elected.


To know more about our bylaws, you may consult the
* [Statutes](/statutes/statuts-201602-en.pdf)
  (or their
  [French
  original](/statutes/statuts-202207-fr.pdf))
* [Interior
Reglementary](/statutes/interieur-201602-en.pdf)
  (or its
  [French
original](/statutes/interieur-201602-fr.pdf))

If you wish to join, please fill in the
[membership
form](/statutes/membershipform.txt)
and send it to the e-mail addresses of the Presidency and the Treasury
given above.


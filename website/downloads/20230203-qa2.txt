These are some notes from the discuussion about patch review this
morning at the Guix Days.


Where to start:
 - qa.guix.gnu.org
 - debbugs.guix.gnu.org
   - issues.guix.gnu.org
   - debbugs.el
 - email
   - personal inbox
   - lei (public inbox) ( https://yhetil.org/guix/ )
 - IRC (someone asking for review)


Applying the patches locally:

 - Pipe it from local email to git am
 - lei emacs package, apply via public inbox and email id
 - cherry pick commits from the branches that qa.guix.gnu.org creates
   - (named issue-XXXXX)


What do people reviewing patches look at?

 - What kind of patch, what is being changed?
 - Testing the changes
   - Look at the results of the qa.guix.gnu.org builds and linter


Once you've applied the patches:

  - Email the bug/submitter
    - Saving drafts as you review multiple patches can be helpful
    - Do you remove guix-patches@gnu.org as a CC? Some do, some don't.

   - Close via debbugs.el

   - Showing the submitter what you've changed as a comitter
     - Use the range diff in Git
     - Copying the Git changes before fixing up/squashing

   - Adding some Fixes: bit to the commit message, maybe we should do
     this more?


Opportunities:

 - Being able to see when the automated testing for an issue (all
   issues) might finish

 - ...

Releases, branches, teams

Problem: It took us 1.5 years for the last release, and the coreupdates
branch has been open for at least as much.


RELEASES

Why do we make releases?
- How else would people install Guix?
- Quality assurance: Fix the installer before release.
- Regular points for translation.
- Communication and publicity.

- Releases are separate from branches; the release team should pick
  a commit and spin off a release branch from there.
- It is possible to have long term support release, mixed with
  more frequent intermediate releases; example from another project:
  "big" release every year, "smaller" releases every 4 months.
- Stable branches are a frequent user request!
- A release schedule for a release every 4 to 6 months sounds
  reasonable.
- Mathieu, Simon, Julien and Andreas volunteer to be members of a release
  team. The idea would be to change the team for each release, with an
  overlap of about half the people: everyone would have "two terms".
  Such a team could commit on a release date and coordinate contributors,
  keep track of the status, ping people fixing bugs etc. Work should
  start immediately after a release to spot problematic areas (poorly
  supported architectures...) and big goals for the next release.
  It could also coordinate with other teams on planned big changes.
  Communication is key! For instance send a status update once per week.
  At the same time, the team should lower their operational involvement
  to avoid burn-out.


BRANCHES

Suggestion:
- Spin off a stable branch from master with security fixes, maybe important
  backports; after 6 months, branch a new stable branch from master; this
  is almost like a release, but continued into some future.

Counter-suggestion:
- Create branches with a few patches or patchsets; in any case with
  a "semantic" description of the changes. The branches could be
  shortlived. Feature branches are one incarnation of the concept.
- The numerical criteria for staging and core-updates is outdated:
  Even non-core packages may create an enormous number of rebuilds.
- Negative point: There is a risk of churn, by not regrouping world-
  rebuilding changes - but two non related world rebuilding changes
  might be difficult to review.

Before creating new branches, we need to clarify how the old branches
are handled!

- Smaller branches could be taken care of by dedicated persons
  responsible for pushing them forward. For instance by teams.
- Some people already do this for a feature branch on their local
  machine for medium-sized updates (ocaml), or even on ci (haskell,
  kernel updates).
- Branch creators should fix a goal and tentative timeline.
- We need a mapping between branches and maintainers responsible
  for the merge. This could be a team leader, if such a role is created.
  A wiki could be used to keep track of the branches.
- There is discussion whether we need a core-updates branch.
  Core updates concern the toolchain, build phase changes, everything
  that has big ramifications all over the system. It would be important
  to not have several "parallel" branches with related (for instance,
  but not exclusively, core-update) changes, which in fact should come
  one after the other. Either they could be collected in one branch,
  or would require coordination of branch creation (inside a team, say).
- "Merge trains" of gitlab are mentioned, as a way of merging several
  branches at the same time.
- Grafts can also be handled in feature branches: The graft is applied
  to master; the graft is applied to a different branch, directly followed
  by an ungrafting commit and an update of the corresponding package;
  once the branch is built it is merged back to master.
- Minor drawback: If qa has treated a world-rebuilding patch, the
  substitutes will be available on bordeaux, but not on berlin; people
  who have installed a long time ago and not authorised bordeaux might
  be affected. If there are complaints, they can be handled on the
  mailing list.
- Moving fast poses problems for non-x86 architectures, but the build farm
  situation has improved for aarch64 and armhf sufficiently to keep up
  with master. Handling feature branches remains an unsolved problem.
- Currently there is a cap on qa, only patches with at most 300 dependents
  are treated. This cap could be increased. Or it could be weighed with
  the build times of the packages.


TEAMS

- Issues could be tagged more often with responsible teams, or with
  severity information (blocking bug or not).
- Each module should be covered by a team; otherwise it would be
  difficult to get important updates through a feature branches.


ARCHITECTURES

- We distinguish in the manual between available architectures that are
  "supported" or "unsupported". It would make sense to have a category
  in-between, with architectures that are supported in principle, but may
  be low on substitute availability.
- We could use the "supported-systems" fields in packages more liberally.


-*- mode: org -*-

#+TITLE: Guix Foundation website

* Building

#+begin_example
  guix shell -C -m manifest.scm \
       -- haunt build
#+end_example

or simply =guix build -f guix.scm=.

* Running

#+begin_example
  guix shell -C -m manifest.scm \
       -- haunt serve -w
#+end_example

Then, visit http://localhost:8080 in a web browser.

You can stop the server pressing ~Ctrl + C~ twice.

* Packing

#+begin_example
  # Generate Docker images
  docker load \
         < $(guix time-machine -q              \
                  --commit=6113e0529d61df7425f64e30a6bf77f7cfdfe5a5 \
                  -- pack                      \
                  -f docker                    \
                  -C none                      \
                  -S /bin=bin                  \
                  -S /lib=lib                  \
                  -S /share=share              \
                  -S /etc=etc                  \
                  -m manifest.scm              \
                  --save-provenance)

  # Tag
  docker tag <IMAGE ID> zimoun/guix-foundation:<my-version>

  # Push
  docker login --username=zimoun
  docker push zimoun/guix-foundation:<my-version>

  # Build
  docker run -v `pwd`:`pwd` -w `pwd` -ti <TAG> \
         haunt build
#+end_example
